function parltrack_download_pipe() {
    [ -n "$CLEAN" ] && rm -rf $1
    [ -f "$1" ] || wget http://parltrack.euwiki.org/dumps/$1 || exit 1

    export DJANGO_SETTINGS_MODULE=memopol.settings
    unxz -c ${OPENSHIFT_DATA_DIR}$1 | $2
    [ -n "$CLEAN" ] && rm -rf $1
}

function francedata_download_pipe() {
    [ -n "$CLEAN" ] && rm -rf $1
    [ -f "$1" ] || wget https://memopol.lqdn.fr/fd/$1 || exit 1

    export DJANGO_SETTINGS_MODULE=memopol.settings
    gunzip -c ${OPENSHIFT_DATA_DIR}$1 | $2
    [ -n "$CLEAN" ] && rm -rf $1
}

function refresh_scores() {
    export DJANGO_SETTINGS_MODULE=memopol.settings
    memopol refresh_scores
}
