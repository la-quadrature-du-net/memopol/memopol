# coding: utf-8


class DelegationHelper:
    '''
    Helper class for building committees/delegations from rep json data
    given dicts for equivalences and abbreviations
    '''

    def __init__(self, equivs, abbrevs, committees=True):
        self.equivs = equivs
        self.abbrevs = abbrevs
        self.committees = committees

    def __call__(self, data):
        items = []
        start = data['mandat_debut']
        end = data.get('mandat_fin', None)

        if self.committees:
            gdata = (i['responsabilite'] for i in data['responsabilites'])
        else:
            gdata = ([i['responsabilite'] for i in data['responsabilites']] +
                [j['responsabilite'] for j in data['groupes_parlementaires']])

        for g in gdata:
            orga = g['organisme']
            role = g['fonction']

            is_committee = orga.lower().startswith('commission') and not (
                orga.lower().startswith(u'commission spéciale') or
                orga.lower().startswith(u'commission d\'enquête')
            )

            if self.committees != is_committee:
                continue

            if orga in self.equivs:
                orga = self.equivs[orga]

            item = {
                'abbr': self.abbrevs[orga] if orga in self.abbrevs else '',
                'name': orga,
                'role': role,
                'start': start
            }

            if end:
                item['end'] = end

            items.append(item)

        return items


def _get_rep_district_name(data):
    num = data.get('num_circo')
    nom = data.get('nom_circo')

    if num == 'nd':
        return nom
    else:
        ordinal = u'ère' if num == 1 else u'ème'
        return '%s (%d%s circonscription)' % (nom, num, ordinal)


_get_sen_committees = DelegationHelper({
    u"COMMISSION DES AFFAIRES EUROPÉENNES Commission des affaires européennes":
        u"Commission des affaires européennes",
    u"Commission de l'aménagement du territoire et du développement durable":
        (u"Commission du développement durable, des infrastructures, de "
            u"l'équipement et de l'aménagement du territoire")
}, {
    u"Commission de la culture, de l'éducation et de la communication":
        "Culture",
    u"Commission des affaires économiques": "Économie",
    u"Commission des affaires étrangères, de la défense et des forces armées":
        "Défense",
    u"Commission des affaires européennes": "Europe",
    u"Commission des affaires sociales": "Social",
    (u"Commission des finances, du contrôle budgétaire et des comptes "
        u"économiques de la nation"): "Finances",
    (u"Commission des lois constitutionnelles, de législation, du suffrage "
        u"universel, du Règlement et d'administration générale"): "Lois",
})

_get_an_committees = DelegationHelper({}, {
    u"Commission de la défense nationale et des forces armées": "Défense",
    u"Commission des affaires culturelles et de l'éducation": "Culture",
    u"Commission des affaires économiques": "Économie",
    u"Commission des affaires étrangères": "Étranger",
    u"Commission des affaires européennes": "Europe",
    u"Commission des affaires sociales": "Social",
    (u"Commission des finances, de l'économie générale et du contrôle "
        u"budgétaire"): "Finances",
    (u"Commission des lois constitutionnelles, de la législation et de "
        u"l'administration générale de la république"): "Lois"
})

_get_sen_delegations = DelegationHelper({}, {}, False)

_get_an_delegations = DelegationHelper({}, {}, False)


#
# Variant configuration
# - mail_domain is used to distinguish official vs personal emails
# - off_* fields are used for the official address of meps
# - mandates defines how mandates are created from the rep json
#
# Mandates are defined as follows
# - 'kind' indicates the group kind, a constant string
# - 'chamber' tells whether the group belongs to the chamber
# - 'from', if present, must be a function that takes the rep json and returns
#   an array of dicts; one group will be created from each item in the dict.
#   When 'from' is not present, only one group wil be created using the rep
#   json (IOW, 'from' defaults to lambda repjson: [repjson])
# - 'name', 'abbr', 'role', 'start', 'end' are strings that are interpolated
#   against the rep json or items returned by 'from'.
# - 'name_path', 'abbr_path', etc. can replace 'name', 'abbr'... by specifying
#   a slash-separated dictionnary path where the value is to be found in the
#   rep json or item returned by 'from'
# - 'name_fn', 'abbr_fn', etc. can also replace 'name', 'abbr'... by a
#   function that takes the input item (rep json or item returned by 'from')
#   and returns the value
#
FranceDataVariants = {
    "AN": {
        "chamber": u"Assemblée nationale",
        "abbreviation": u"AN",
        "chamber_url_field": u"url_an",
        "mail_domain": u"@assemblee-nationale.fr",
        "off_city": u"Paris",
        "off_street": u"Rue de l'Université",
        "off_number": u"126",
        "off_code": u"75355",
        "off_name": u"Assemblée nationale",
        "mandates": [
            {
                "kind": u"chamber",
                "chamber": True,
                "abbr": u"AN",
                "name": u"Assemblée nationale",
                "role": u"Député",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"group",
                "chamber": True,
                "abbr": u"%(groupe_sigle)s",
                "name_path": u"groupe/organisme",
                "role_path": u"groupe/fonction",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"department",
                "abbr": u"%(num_deptmt)s",
                "name": u"%(nom_circo)s",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"district",
                "abbr": u"%(num_deptmt)s-%(num_circo)s",
                "name_fn": _get_rep_district_name,
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"committee",
                "chamber": True,
                "from": _get_an_committees,
                "abbr": u"%(abbr)s",
                "name": u"%(name)s",
                "role": u"%(role)s",
                "start": u"%(start)s",
                "end": u"%(end)s"
            },
            {
                "kind": u"delegation",
                "chamber": True,
                "from": _get_an_delegations,
                "abbr": u"%(abbr)s",
                "name": u"%(name)s",
                "role": u"%(role)s",
                "start": u"%(start)s",
                "end": u"%(end)s"
            }
        ]
    },

    "SEN": {
        "chamber": u"Sénat",
        "abbreviation": u"SEN",
        "chamber_url_field": u"url_institution",
        "mail_domain": u"@senat.fr",
        "off_city": u"Paris",
        "off_street": u"Rue de Vaugirard",
        "off_number": u"15",
        "off_code": u"75291",
        "off_name": u"Palais du Luxembourg",
        "mandates": [
            {
                "kind": u"chamber",
                "chamber": True,
                "abbr": u"SEN",
                "name": u"Sénat",
                "role": u"Sénateur",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"group",
                "chamber": True,
                "abbr": u"%(groupe_sigle)s",
                "name_path": u"groupe/organisme",
                "role_path": u"groupe/fonction",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"department",
                "abbr": u"%(num_deptmt)s",
                "name": u"%(nom_circo)s",
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"district",
                "abbr": u"%(num_deptmt)s-%(num_circo)s",
                "name_fn": _get_rep_district_name,
                "start": u"%(mandat_debut)s",
                "end": u"%(mandat_fin)s"
            },
            {
                "kind": u"committee",
                "chamber": True,
                "from": _get_sen_committees,
                "abbr": u"%(abbr)s",
                "name": u"%(name)s",
                "role": u"%(role)s",
                "start": u"%(start)s",
                "end": u"%(end)s"
            },
            {
                "kind": u"delegation",
                "chamber": True,
                "from": _get_sen_delegations,
                "abbr": u"%(abbr)s",
                "name": u"%(name)s",
                "role": u"%(role)s",
                "start": u"%(start)s",
                "end": u"%(end)s"
            }
        ]
    }
}
